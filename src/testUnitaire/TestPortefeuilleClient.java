package testUnitaire;

import mainPackage.CompteCourant;
import mainPackage.PortefeuilleClients;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestPortefeuilleClient
{
    PortefeuilleClients portefeuille = new PortefeuilleClients();

    @Test
    void addCompte()
    {
        assertEquals(0, portefeuille.getNbCompte(), "Erreur sur le nombre initials de compte dans le portefeuille client");
        portefeuille.addCompte(new CompteCourant("Bryan", 1234, 5303, 50));
        assertEquals(1, portefeuille.getNbCompte(), "Le compte n'a pas correctement été inséré au portefeuille client");
    }

    @Test
    void removeCompte()
    {
        portefeuille.addCompte(new CompteCourant("Bryan", 1234, 5303, 50));
        assertEquals(1, portefeuille.getNbCompte(), "Erreur lors de l'ajout d'un compte client avant suppression");

        portefeuille.removeClient(1234);
        assertEquals(0, portefeuille.getNbCompte(), "Le compte n'a pas correctement été supprimé du portefeuille client");
    }
}
