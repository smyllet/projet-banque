package testUnitaire;

import mainPackage.CompteCourant;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCompteCourant
{
    @Test
    void getSolde()
    {
        CompteCourant compte1 = new CompteCourant("Bryan", 1234, 5303, 50);

        assertEquals(5303, compte1.getSolde(), "Le solde initials du compte est incorrect");
    }

    @Test
    void addToSolde()
    {
        CompteCourant compte1 = new CompteCourant("Bryan", 1234, 5303, 50);
        compte1.addToSolde(100);
        assertEquals(5403, compte1.getSolde(), "Erreur lors de l'ajout au solde");
    }

    @Test
    void removeToSolde()
    {
        CompteCourant compte1 = new CompteCourant("Bryan", 1234, 5303, 50);
        compte1.removeToSolde(100);
        assertEquals(5203, compte1.getSolde(), "Erreur de retrait au solde");
    }
}
