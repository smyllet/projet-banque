package mainPackage;

public class UseCompteCourant
{
    public static void main(String[] args)
    {
        //Création d'un portefeuille client
        PortefeuilleClients comptes = new PortefeuilleClients();

        //Ajout compte par saisie de valeur
        //comptes.addCompte(3);

        //Ajout de deux client
        CompteCourant jimmy = new CompteCourant("Jimmy", 100, -20, 0);
        comptes.addCompte(jimmy);
        CompteCourant abdou = new CompteCourant("Abdou", 200, 47, 100);
        comptes.addCompte(abdou);
        CompteCourant theo = new CompteCourant("Théo", 300, -444, 250);
        comptes.addCompte(theo);
        CompteCourant bryan = new CompteCourant("Bryan", 400, -667, 0);
        comptes.addCompte(bryan);
        CompteCourant clement = new CompteCourant("Clément", 500, 120, 100);
        comptes.addCompte(clement);

        //Afficher le nombre de client
        System.out.println("Le portefeuille Client contient " + comptes.getNbCompte() + " client(s).");

        //Liste des compte
        System.out.println(comptes.toString());

        //Liste des compte à découvert
        System.out.println(comptes.getDecouvertListe());

        //Supression d'un compte
        comptes.removeClient(300);

        //Liste des client après supression
        System.out.println("Supression du compte de théo (numéro 300)");
        System.out.println(comptes.toString());
    }
}
