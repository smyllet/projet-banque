package mainPackage;

public class UseCompte
{
    public static void main(String[] args) {
        System.out.println("Compte de Dupond");
        Compte dupond = new Compte("Dupond", 1234, 450);
        System.out.println("Solde : " + dupond.getSolde());
        dupond.addToSolde(500);
        System.out.println("Solde : " + dupond.getSolde());
        dupond.removeToSolde(120);
        System.out.println("Solde : " + dupond.getSolde());

        System.out.println("\nCompte de Martin");
        Compte martin = new Compte("Martin", 4567, 150);
        System.out.println("Solde : " + martin.getSolde());
        martin.addToSolde(500);
        System.out.println("Solde : " + martin.getSolde());
        martin.removeToSolde(1000);
        System.out.println("Solde : " + martin.getSolde());

        System.out.println("\nCompte de Dupond");
        System.out.println("Solde : " + dupond.getSolde());
    }
}
