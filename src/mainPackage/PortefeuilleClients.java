package mainPackage;

import java.util.ArrayList;

public class PortefeuilleClients
{
    private ArrayList<CompteCourant> listeComptes; //Liste des comptes

    //Constructeur
    public PortefeuilleClients()
    {
        listeComptes = new ArrayList<CompteCourant>();
    }

    //Ajouter des compte par saisie de valeur
    public void addCompte(int nb)
    {
        for(int i=0; i<nb; i++)
        {
            CompteCourant pp = new CompteCourant();
            listeComptes.add(pp);
        }
    }

    //Ajouter de compte via paramètre
    public void addCompte(CompteCourant compte)
    {
        listeComptes.add(compte);
    }

    //Optenir le nombre de compte
    public int getNbCompte()
    {
        return listeComptes.size();
    }

    //Optenir les des comptes
    public String toString()
    {
        String result = "Liste des clients : \n";

        for (CompteCourant compte : listeComptes)
        {
            result = result + compte.toString() + "\n";
        }

        return result;
    }

    //Optenir la liste des client à découvert
    public String getDecouvertListe()
    {
        String result = "Liste des clients à découvert : \n";

        for (CompteCourant compte : listeComptes)
        {
            if(compte.getSolde()+compte.autorisationDecouvert < 0)
            {
                result = result + "Le compte numéro " + compte.numero + " appartement à M/Mme " + compte.titulaire + " est à découvert de " + compte.getSolde() + "€ pour un découvert autorisé de " + compte.autorisationDecouvert + "€ soit " + Math.abs(compte.getSolde() + compte.autorisationDecouvert) + "€ de plus que sont découvert autorisé" + "\n";
            }
        }

        return result;
    }

    public void removeClient(int numeroClient)
    {
        listeComptes.removeIf(compte -> compte.numero == numeroClient);
    }
}
