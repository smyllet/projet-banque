package mainPackage;

import Utils.Saisie;

public class CompteCourant
{
    String titulaire;
    int numero;
    double autorisationDecouvert;
    private double solde;

    public double getSolde()
    {
        return solde;
    }

    public void addToSolde(double ajout)
    {
        this.solde = this.solde + ajout;
    }

    public void removeToSolde(double retrait)
    {
        if((this.getSolde() - retrait + this.autorisationDecouvert) > 0)
        {
            this.solde = this.solde - retrait;
        }

    }

    public CompteCourant(String titulaire, int numero, double solde, double autorisationDecouvert)
    {
        this.titulaire = titulaire;
        this.numero = numero;
        this.solde = solde;
        this.autorisationDecouvert = autorisationDecouvert;
    }

    public CompteCourant()
    {
        this.titulaire = Saisie.lire_String("Veuillez saisir votre nom");
        this.numero = Saisie.lire_int("Veuillez saisir votre numero de compte");
        this.solde = Saisie.lire_double("Veuillez saisir votre solde");
        this.autorisationDecouvert=0;
    }

    public String toString()
    {
        return "Numéro de compte : " + numero + ", Titulaire : " + titulaire + ", Solde : " + solde + ", Autorisation de découvert : " + autorisationDecouvert;
    }
}
