package mainPackage;

public class Compte
{
    String titulaire;
    int numero;
    private double solde;

    public double getSolde()
    {
        return solde;
    }

    public void addToSolde(double ajout)
    {
        this.solde = this.solde + ajout;
    }

    public void removeToSolde(double retrait)
    {
        this.solde = this.solde - retrait;
    }

    public Compte(String titulaire, int numero, double solde)
    {
        this.titulaire = titulaire;
        this.numero = numero;
        this.solde = solde;
    }
}
