package mainPackage;

import Utils.Saisie;

public class UseCompteAdvanced
{
    public static void main(String[] args)
    {
        String input;
        String subInput;

        PortefeuilleClients comptes = new PortefeuilleClients();

        do {
            input = Saisie.lire_String("Action (quitter, portefeuille, compte) : ");
            switch (input)
            {
                case "portefeuille":
                    subInput = Saisie.lire_String("Portefeuille - Action (liste, découvert, ajouter, suprimer) : ");
                    switch (subInput)
                    {
                        case "liste":
                            System.out.println(comptes.toString());
                            break;
                    }
                    break;
                case "compte":
                    break;
                case "quitter":
                    System.out.println("A bientot !");
                    break;
                default:
                    System.out.println("Commande incorrect");
                    break;
            }
        }while(!input.equals("quitter"));
    }
}
